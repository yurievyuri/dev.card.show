<?php

use Dev\Call\Card;
use Bitrix\Main\Config\Option;

define ('NO_KEEP_STATISTIC', 'Y');
define ('NO_AGENT_STATISTIC', 'Y');
define ('NO_AGENT_CHECK', true);
define ('STOP_STATISTICS', true);
define ('PUBLIC_AJAX_MODE', true);
define ('DisableEventsCheck', true);

require_once ( $_SERVER[ 'DOCUMENT_ROOT' ] . '/bitrix/modules/main/include/prolog_before.php' );

if ( !$_REQUEST[ 'PLACEMENT' ] ) {
    $appData = [
        'TITLE' => 'EXTRA',
        'COMMENT' => 'Additional data management features during a call', // aka description
        //'GROUP_NAME' => 'group name'
        //'ADDITIONAL'
    ];
    return;
}
if ( empty($_POST) ) {
    return;
}

global $USER, $APPLICATION;
if ( !$USER->IsAuthorized () ) {
    die('Not correct auth');
}

if ( $_REQUEST[ 'PLACEMENT_OPTIONS' ] ) {
    $_REQUEST[ 'PLACEMENT_OPTIONS' ] = json_decode($_REQUEST[ 'PLACEMENT_OPTIONS' ], true );
}

if ( !class_exists (Card::class) ) {
    \Bitrix\Main\Loader::includeModule ('dev.card.show');
}
$module_id = \Dev\Call\Card::module;
$prefix = \Dev\Call\Card::prefix;

$debugMode = \Bitrix\Main\Config\Option::get ($module_id, $prefix . 'debug') === 'Y' && $USER->IsAdmin ();
$debugLabel = 'debugTime';
if ( $debugMode ) {
    \Bitrix\Main\Diag\Debug::startTimeLabel ($debugLabel);
}
?>
<!doctype html >
<html>
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
</head>
<body class="index" ontouchstart>
<?php

if ( empty($_REQUEST[ 'PLACEMENT_OPTIONS' ]) ) {
    echo 'No input received. [PLACEMENT_OPTIONS] is EMPTY. Execution terminated.';
    return;
}
$APPLICATION->ShowHeadStrings ();
$APPLICATION->ShowHeadScripts ();
$APPLICATION->showHead ();
$APPLICATION->IncludeComponent (
    'bitrix:crm.card.show.ext',
    '.default',
    [
        'ENTITY_TYPE' => $_REQUEST[ 'PLACEMENT_OPTIONS' ][ 'CRM_ENTITY_TYPE' ],
        'ENTITY_ID' => $_REQUEST[ 'PLACEMENT_OPTIONS' ][ 'CRM_ENTITY_ID' ],
        'PHONE_NUMBER' => $_REQUEST[ 'PLACEMENT_OPTIONS' ][ 'PHONE_NUMBER' ],
        //'RESULT' => $arResult,
        'PARAMS' => $_REQUEST[ 'PLACEMENT_OPTIONS' ],
        'ERRORS' => false,
        'DEBUG' => $debugMode
    ]
);

if ( $debugMode ) {
    \Bitrix\Main\Diag\Debug::endTimeLabel ($debugLabel);
    echo '<font color="white" style="background: red; position: absolute; bottom:23px; left:20px; opacity: 0.5; z-index: 105;">Execute Time: ' . round (\Bitrix\Main\Diag\Debug::getTimeLabels ()[ $debugLabel ][ 'time' ], 3) . '</font>';
}

require_once ( $_SERVER[ 'DOCUMENT_ROOT' ] . '/bitrix/modules/main/include/epilog_after.php' ); ?>

</body>
</html>