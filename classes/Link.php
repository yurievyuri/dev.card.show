<?php
namespace Dev\Call;
use Bitrix\Crm\Automation\Converter\Converter;
use Bitrix\Crm\FieldMultiTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\LoaderException;

/**
 * @property array REQUEST
 * @property mixed PARAMS
 * @property  parentId
 * @property mixed parentId
 * @property int IBLOCK_ID
 * @property string CONTACT_BIND_PROPERTY
 */
class Link {

    const KEY_MAIN = 'LINK';
    const KEY_MAIN_TYPE = 'CONTACT';
    const MODULE = 'dev.card.show';
    const PREFIX = 'ahc_card_';

    private $ERROR;
    private $RESULT;

    public function __construct( $request = [] )
    {
        if ( !is_array($request) ) return;
        $this->REQUEST = $request;

        $this->IBLOCK_ID = Option::get(self::MODULE, self::PREFIX . "prop_bind_link_iblock") ? : 61;
        $this->CONTACT_BIND_PROPERTY = Option::get(self::MODULE, self::PREFIX .  "prop_contact_bind_type") ? : 'UF_CRM_1582036405';
    }

    /**
     * Setting Parent ID
     * @param bool $id
     * @return mixed
     */
    public function setParentId( $id = null )
    {
        if ( !$id )
            $this->parentId = $this->REQUEST['ENTITY_ID'];
        else
            $this->parentId = (int) $id;

        return $this->getParentId();
    }

    /**
     *  Getting Parent ID
     * @return mixed
     */
    public function getParentId()
    {
        if ( !$this->parentId )
            $this->parentId = $this->REQUEST['ENTITY_ID'];

        return $this->parentId ;
    }

    /**
     * Link Array Processing
     * @param null $parentId
     * @return bool
     */
    public function inboundProcessing( $parentId = null )
    {
        if ( !$this->сomplianceCheck() ) return false;

        if ( !$parentId )
            $this->getParentId();
        else
            $this->setParentId($parentId);

        foreach ( $this->PARAMS as $type => $links )
        {   if ( $type != self::KEY_MAIN_TYPE ) continue;
            foreach( $links as $linkId => $rel )
                $this->addNewLink($linkId,$rel);
        }
    }

    private function addNewLink( $id = null, $rel = null )
    {
        if ( !$id || !$rel || !$this->getParentId() )
        {
            $this->setERROR('Not enough data to save to infoblock', __METHOD__);
            return null;
        }

        if ( !class_exists('\CIBlockElement') )
            \Bitrix\Main\Loader::includeModule('iblock');

        try {
            $this->updateCrmContactData($id);

        } catch (LoaderException $e) {
            
            $this->setERROR($e, __METHOD__);
        }

        $values = [
            'ID' => $id,
            'NAME' => $rel,
            'PARENT_ID' => $this->getParentId()
        ];

        if ( $this->checkDublicate($values) )
        {
            $this->setERROR('An item with these criteria already exists', __METHOD__);
            return false;
        }

        global $USER;
        $array = Array(
          "MODIFIED_BY"         =>  $USER->GetID(),
          "IBLOCK_SECTION_ID"   =>  false,
          "IBLOCK_ID"           =>  $this->IBLOCK_ID,
          "PROPERTY_VALUES"     =>  $values,
          "NAME"                =>  $id,
          "ACTIVE"              =>  "Y"
        );

        $el = new \CIBlockElement();

        $this->RESULT['CREATED'][] = $el->Add( $array );
        if( $el->LAST_ERROR )
            $this->setERROR($el->LAST_ERROR, __METHOD__);
    }

    /**
     * @param null $linkId
     * @return bool
     * @throws \Bitrix\Main\LoaderException
     */
    private function updateCrmContactData( $linkId = null )
    {
        if ( !$linkId ) return false;

        if ( !class_exists('\CCrmContact') )
            \Bitrix\Main\Loader::includeModule('crm');

        $contact_rel_arr = \CCrmContact::GetList
        (
            array(),
            array('=ID' => $this->getParentId()),
            array($this->CONTACT_BIND_PROPERTY, 'TYPE_ID')
        )->Fetch()[$this->CONTACT_BIND_PROPERTY];

        if ( in_array($linkId, $contact_rel_arr) ) return false;

        $contact_rel_arr[] = $linkId;
        $arFields = [$this->CONTACT_BIND_PROPERTY => $contact_rel_arr];
        $CCrmContact = new \CCrmContact(false);
        $this->REQUEST['UPDATE']['CONTACT'][$this->getParentId()] = $CCrmContact->Update($this->getParentId(), $arFields);
        if( $CCrmContact->LAST_ERROR )
            $this->setERROR($CCrmContact->LAST_ERROR, __METHOD__);
    }

    private function checkDublicate( $array )
    {
        if ( !class_exists('\CIBlockElement') )
            \Bitrix\Main\Loader::includeModule('iblock');

        $filter = [
            'IBLOCK_ID'         =>  $this->IBLOCK_ID,
            'ACTIVE'            =>  'Y',
            'CHECK_PERMISSIONS' =>  'N'
        ];

        foreach ( $array as $name => $value )
            $filter[ 'PROPERTY_' . $name ] = $value;

        #old school
        $db = \CIBlockElement::GetList(
            [   'ID'=>'DESC'   ],
            $filter,
            false,
            ['nTopCount' => 1],
            ['ID']
        )->Fetch();

        return $this->RESULT['DUBLICATES'][$db['ID']] = $db ? true : false;

    }

    /**
     * Input Validation
     * @return bool
     */
    private function сomplianceCheck() : bool
    {
        if ( !$this->REQUEST[ self::KEY_MAIN ] )
        {
            //$this->setRESULT( 'Required input to execute not found', __METHOD__ );
            return false;
        }

        $this->getRequestData();

        return true;
    }

    private function getRequestData() : array
    {
        return $this->PARAMS = $this->REQUEST[ self::KEY_MAIN ];
    }

    public function setERROR($data, $key = false )
    {
        if ( $key && is_string($data))
            $this->ERROR[] = $data . ' :: ' .$key ;
        elseif ( $key && (is_array($data) || is_object($data)))
            $this->ERROR[$key] = $data ;
        else
            $this->ERROR[] = $data ;
    }

    /**
     * @return mixed
     */
    public function getERROR()
    {
        return $this->ERROR;
    }
}