<?php
namespace Dev\Call;

use Bitrix\Crm\Integration\Application;
use Bitrix\Crm\Integration\Rest\AppPlacement;
use \Bitrix\Rest\AppTable;
use Bitrix\Rest\PlacementTable;

/**
 * Class AppPlacements
 * @package Dev\Call
 */
class AppPlacements extends Card
{
    /**
     * @var
     */
    private $ERROR;
    /**
     * @var mixed
     */
    private $root;
    /**
     * @var string|string[]
     */
    private $dirModule;
    /**
     * @var string
     */
    private $dirPlacement;
    /**
     * @var array
     */
    private $filesData;
    /**
     * @var array
     */
    private $filesPath;
    /**
     * @var
     */
    private $appId;
    /**
     * @var array
     */
    private $placementList;
    /**
     * @var array
     */
    private $existPlacementHandlers;
    /**
     * @var
     */
    private $placementListHandlerDb;
    /**
     * @var array
     */
    private $deletedPlacements;
    /**
     * @var array
     */
    private $addedPlacements;
    /**
     * @var array
     */
    private $unInsalledApp;

    /**
     * AppPlacements constructor.
     * @param array $option
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function __construct($option = [])
    {
        $this->root = $_SERVER['DOCUMENT_ROOT'];
        $this->dirModule = str_ireplace('/classes', '', __DIR__);
        $this->dirPlacement = $this->dirModule . '/placements/';

        $this->getFilesData();
        $this->checkInstalledApp();
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function install()
    {
        $this->installRestApp();
        $this->getPlacementsList();
        $this->addPlacements();
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function uninstall()
    {
        $this->deletePlacements();
        $this->uninstallRestApp();
    }

    /**
     * @return bool|mixed
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function checkInstalledApp()
    {
        if ( $this->appId )
            return $this->appId;

        if ( !parent::appname )
        {
            $this->setERROR(__METHOD__, 'Missing specified application name');
            return false;
        }

        $this->appId = AppTable::getList([
            'order' => ['ID'=>'DESC'],
            'filter' => [ /*'APP_NAME' => self::appname ,*/ 'USER_INSTALL' => 'N', 'APPLICATION_TOKEN' => parent::appcode ]
        ])->fetch()['ID'];

        return $this->appId ? $this->appId : false;
    }

    /**
     * @return array|bool|int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function installRestApp()
    {
        if ( $this->checkInstalledApp() )
        {
            $this->setERROR(__METHOD__, 'The application is already installed ' . $this->appId );
            return false;
        }

        $array = array (
            //'CLIENT_ID' => 'local.5e4415e3209ca2.82390222',
            //'CODE' => parent::appcode, //'local.5e4415e3209ca2.82390222',
            'ACTIVE' => 'Y',
            'INSTALLED' => 'Y',
            //'URL' => 'https://ahc.dev/pub/x.php',
            'URL_DEMO' => NULL,
            'URL_INSTALL' => '',
            'VERSION' => '1',
            'SCOPE' => 'telephony,call,pay_system,crm,imopenlines,timeman,im,imbot,task,tasks_extended,placement,user,entity,pull,pull_channel,mobile,forum,calendar,messageservice,log,sonet_group,mailservice,lists,faceid,sale,bizproc,documentgenerator,userconsent,smile,landing,disk,department,contact_center,catalog,intranet,salescenter,socialnetwork',
            'STATUS' => 'L',
            'DATE_FINISH' => NULL,
            'IS_TRIALED' => 'N',
            //'SHARED_KEY' => '0c5917d2db777937bbf071f7cb4f065e',
            //'CLIENT_SECRET' => 'f6P5fnCJZOAll9MhedpvJ8Qq7JxSVSJfW52zlp2AWeAXdJPcKm',
            'APP_NAME' => parent::appname,
            'ACCESS' => NULL,
            'APPLICATION_TOKEN' => parent::appcode,
            'MOBILE' => 'N',
            'USER_INSTALL' => 'N',
        );

        $result = AppTable::add($array);
        if($result->isSuccess())
            return $this->appId = $result->getId();
    }

    /**
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function uninstallRestApp()
    {
        if ( !$this->checkInstalledApp() )
        {
            $this->setERROR(__METHOD__, 'Uninstall application not found' );
            return false;
        }

        return $this->unInsalledApp[] = AppTable::delete( $this->appId )->isSuccess();
    }

    /**
     * @return bool
     */
    public function getFiles()
    {
        if ( !is_dir($this->dirPlacement) )
        {
            $this->setERROR(__METHOD__,'not exist placements folder');
            return false;
        }

        foreach ( glob($this->dirPlacement.'*.php') as $file )
            $this->filesPath[] = $file;
    }

    /**
     * @return bool
     */
    public function getFilesData()
    {
        $this->getFiles();

        if ( !$this->filesPath ){

            $this->setERROR(__METHOD__,'not exist files');
            return false;
        }

        foreach ( $this->filesPath as $path )
        {
            $appData = false;
            if ( !file_exists($path)) continue;
            require_once($path);
            if($appData)
            {
                $appData['PLACEMENT_HANDLER'] = $this->existPlacementHandlers[] = str_ireplace( $this->root, '', $path );
                $this->filesData[] = $appData;
            }
        }
    }

    /**
     * @param $key
     * @param $data
     * @return bool
     */
    public function setERROR($key, $data ) : bool
    {
        $this->ERROR[$key] = $data;
        return true;
    }

    /**
     * @return mixed
     */
    public function getERROR()
    {
        return $this->ERROR;
    }

    /**
     * @param array $array
     * @throws \Exception
     */
    public function createPlacement(array $array )
    {
        $placementBind = array(
            'APP_ID'    => $this->appId,
            'PLACEMENT' => parent::placement
        );
        $placementBind = array_merge( $placementBind, $array );
        $this->addedPlacements[] = PlacementTable::add($placementBind)->getId();
    }

    /**
     * @throws \Exception
     */
    public function deletePlacements()
    {
        foreach($this->getPlacementsList() as $value ){
            $this->deletedPlacements[] = PlacementTable::delete($value['ID'])->isSuccess();
        }
    }

    /**
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function addPlacements()
    {
        if ( !$this->checkInstalledApp() )
        {
            $this->setERROR(__METHOD__, 'Installed application not found' );
            return false;
        }

        if ( !$this->filesPath )
            $this->getFilesData();

        $this->getPlacementsList();

        foreach ($this->filesData as $value )
        {
            if ( in_array( $value['PLACEMENT_HANDLER'], $this->placementListHandlerDb ) ) continue;

            $this->createPlacement($value);
        }
    }

    /**
     * @param string|NULL $handlerPath
     * @return array|bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getPlacementsList(string $handlerPath = NULL )
    {
        if ( !$this->checkInstalledApp() )
        {
            $this->setERROR(__METHOD__, 'Installed application not found' );
            return false;
        }

        $filter = [

            'PLACEMENT' => parent::placement, 'APP_ID' =>  $this->appId
        ];

        if ( $this->existPlacementHandlers )
            $filter['PLACEMENT_HANDLER'] = $this->existPlacementHandlers;

        if ( $handlerPath !== NULL )
            $filter['PLACEMENT_HANDLER'] = $handlerPath;

        $db = PlacementTable::getList([

            'filter' => $filter,
            'limit' => 100

        ])->fetchAll();

        foreach ( $db as $value )
        {
            $this->placementList[] = $value;
            $this->placementListHandlerDb[] = $value['PLACEMENT_HANDLER'];
        }

        $this->placementListHandlerDb = array_unique($this->placementListHandlerDb);

        return $this->placementList;
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->appId;
    }
}