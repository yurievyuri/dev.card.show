# DEV.CARD.SHOW
`Custom Bitrix Module with Rest Placement Application`

This application allows you to significantly increase the ability to manage leads from the standard pop-up window component during a call.

#### version 1.1.6
_2020-03-26_
* json decode fix

#### version 1.1.5
_2020-03-25_

* Fixed a bug preventing the correct choice of the person responsible for the lead in the drop-down list
* Added log entry $arResult in debug mode (/local/modules/#MODULE_NAME#.log)

#### version 1.1.4
_2020-03-25_

* Fixed error output in component

* Fixed bug display error in debug mode

* A few minor fixes that do not affect application performance

````
Update app and be sure to re-configure the settings in the administrative panel
````

**Yuri Yuriev** - *AHC* - [Yuri Yuriev](https://gitlab.com/yurievyuri/)
* PHPStorm 2019.3.4
* PHP 7.2
* Vanilla JS ESMAScript 6
* Bitrix24 Api
* Bitrix24 Rest Api
* Semantic UI 2.4.2
* jQuery 3.4.1 (exclusively for Semantic UI)