<?php
$group = explode(',', \Bitrix\Main\Config\Option::get('dev.card.show', "ARRAY_USER_GROUPS")) ? : [1,25];
if(empty($group)) $group = [1,25];
if ( !is_object($USER) )global $USER;
if ( !array_intersect($group, $USER->GetUserGroupArray()) )
    \Bitrix\Main\Page\Asset::getInstance()->addJs('/local/modules/dev.card.show/js/card_patch.js');