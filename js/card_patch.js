document.addEventListener("DOMContentLoaded", patchCallCard);
function patchCallCard() {
    if ( !BX ) return false;
    BX.PhoneCallView.setDefaults({restApps: []});
    BX.addCustomEvent( 'CallCard::EntityChanged', function(data)
    {
        BX.PhoneCallView.setDefaults({restApps: []});
        console.log('card is patched');
    });
    /*BX.addCustomEvent( 'onPullEvent-voximplant', function(data)
    {
        BX.PhoneCallView.setDefaults({restApps: []});
    });*/
}