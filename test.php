<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
global $USER;
if( !$USER->IsAdmin() ) {
    die('Only Admin');
}
$array = [
  'REST_USER_ID',
  'USER_ID',
  'REST_ID',
  'PHONE'
];
?>

<form action="<?=$_SERVER['SCRIPT_URL']?>">
    <?
    foreach( $array as $request )
    {
        if( $_REQUEST[$request] )
            \Bitrix\Main\Config\Option::set('dev.card.show', $request.'mem', $_REQUEST[$request]);
        else
            $_REQUEST[$request] = \Bitrix\Main\Config\Option::get('dev.card.show', $request.'mem') ? : false;

        echo '<input name="'.$request.'" value="'.$_REQUEST[$request].'" />';
    }
    ?>
    <button type="submit">Go on!</button>
</form>

<?php

$queryUrl = 'https://' . $_SERVER['SERVER_NAME'] . '/rest/'.$_REQUEST['REST_USER_ID'].'/'.$_REQUEST['REST_ID'].'/telephony.externalcall.register';

echo '<pre>'; print_r( $queryUrl ); echo '</pre>';

$queryData = http_build_query(array(
    'USER_PHONE_INNER' => 111,
    'USER_ID' => $_REQUEST['USER_ID'],
    'PHONE_NUMBER' => $_REQUEST['PHONE'],
    'TYPE' => 2,
    'CALL_START_DATE' => date('Y-m-d H:i:s'),
    'CRM_CREATE' => true,
    'SHOW'  => true
));

if ( !$_REQUEST['REST_ID'] ) die();

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
));

$result = curl_exec($curl);
echo '<pre>'; print_r( curl_error($curl) ); echo '</pre>';

curl_close($curl);
$result = json_decode($result, 1);

echo '<pre>'; print_r( $result ); echo '</pre>';