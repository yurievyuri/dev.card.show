<?php
$columnClass = 'eight wide column mini';
?>
<div class="ui equal width grid">

    <div class="<?=$columnClass?>">
        <div class="field">
        <?=$component->createInputUI('NAME', $arResult, 0, true)?>
        </div>
    </div>

    <div class="<?=$columnClass?>">
        <div class="field">
            <?=$component->createInputUI('LAST_NAME', $arResult, 0)?>
        </div>
    </div>

    <div class="<?=$columnClass?>">

        <?$propName = \Bitrix\Main\Config\Option::get($module_id, $prefix . 'prop_lead_type');?>
        <div class="field">
            <label><small><?=$component->prefetchCorrectFieldName($propName, $arResult)?></small></label>
            <?=CrmCardShowExtComponent::createSelectUI($propName, $arResult, 2, false, 'drpdwn')?>
        </div>

    </div>

    <div class="<?=$columnClass?>">

        <?$propName = \Bitrix\Main\Config\Option::get($module_id, $prefix . 'prop_market_type');?>
        <div class="field">
            <label><small><?=$component->prefetchCorrectFieldName($propName, $arResult)?></small></label>
            <?=CrmCardShowExtComponent::createSelectUI($propName, $arResult, 3, false, 'drpdwn' )?>
        </div>

    </div>


    <div class="<?=$columnClass?>">

        <div class="field">
            <?$propName = 'ASSIGNED_BY_ID';?>
            <label><small><?=$component->prefetchCorrectFieldName($propName, $arResult)?></small></label>
            <?=CrmCardShowExtComponent::createSelectUI($propName, $arResult, 4, false, 'rmt')?>
        </div>

    </div>

    <div class="<?=$columnClass?>">

        <?$propName = 'SOURCE_ID';?>
        <div class="field">
            <label><small><?=$component->prefetchCorrectFieldName($propName, $arResult)?></small></label>
            <?=CrmCardShowExtComponent::createSelectUI($propName, $arResult, 5, false, 'drpdwn')?>
        </div>

    </div>
</div>