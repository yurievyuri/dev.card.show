<div type="folder" class="<?=pathinfo(__FILE__)['filename']?>" style="display:none">

    <h5 class="ui header">
        <i class="<?=explode('|',$additionalButtons['link'])[1]?>"></i>
        <div class="content">
            Link Search:
        </div>
    </h5>

    <div class="inline fields">
        <label>Filter:</label>
        <?=$component->createUIContactTypeFilter(true);?>
    </div>

    <div class="field">
        <div class="ui fluid category search link">
            <div class="ui icon input">
                <input class="prompt" type="text" name="LINK_SEARCH" placeholder="Search contacts by name, phone, email, id...">
                <i class="search icon"></i>
            </div>
            <div class="results"></div>
        </div>
    </div>

    <div class="ui two cards field linkFieldCards"></div>

</div>