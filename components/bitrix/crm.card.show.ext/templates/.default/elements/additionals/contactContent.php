<div type="folder" class="<?=pathinfo(__FILE__)['filename']?>" style="display:none">

    <h5 class="ui header">
        <i class="<?=explode('|',$additionalButtons['contact'])[1]?>"></i>
        <div class="content">
            Convert Current Lead to Contact
        </div>
    </h5>

    <div class="field">
        <label><small>Contact Type:</small></label>
        <div class="ui fluid search selection dropdown drpdwn" id="<?=pathinfo(__FILE__)['filename']?>">
            <input type="hidden" name="UPDATE[CONTACT][TYPE_ID]" value="">
            <div class="default text">Select a Contact Type</div>
            <i class="dropdown icon"></i>
            <div class="menu" style="max-height: 21rem;">
                <? foreach( $arResult['CONTACT_TYPES'] as $id => $name ):?>
                    <div class="item" data-value="<?=$id?>"><?=$name;?></div>
                <?endforeach;?>
            </div>
        </div>
    </div>

    <div class="field m-t-30">
        <div class="ui toggle checkbox">
            <input type="checkbox" name="MOVE_LEAD" value="">
            <label>Confirm Action</label>
        </div>
        <div class="ui toggle checkbox" style="margin-left: 20px;">
            <input type="checkbox" name="DELETE_LEAD" value="">
            <label>Delete Incoming Lead</label>
        </div>
    </div>

</div>