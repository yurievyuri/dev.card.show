<table class="inputExtraForm" style="margin-bottom: 10px;">

    <tr>
        <td>
            <div class="field">
                <?=$component->createInputUI('NAME', $arResult, 0, true);?>
            </div>
        </td>
        <td>
            <?$propName = 'LAST_NAME';?>
            <div class="field">
                <?=$component->createInputUI('LAST_NAME', $arResult, 0);?>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <?$propName = \Bitrix\Main\Config\Option::get($module_id, $prefix . "prop_lead_type");?>
            <div class="field">
                <label><small><?=$component->prefetchCorrectFieldName($propName, $arResult);?></small></label>
                <?=CrmCardShowExtComponent::createSelectUI($propName, $arResult, 2);?>
            </div>
        </td>
        <td>
            <?$propName = \Bitrix\Main\Config\Option::get($module_id, $prefix . "prop_market_type");?>
            <div class="field">
                <label><small><?=$component->prefetchCorrectFieldName($propName, $arResult);?></small></label>
                <?=CrmCardShowExtComponent::createSelectUI($propName, $arResult, 3);?>
            </div>
        </td>
    </tr>

    <tr>
        <td>
            <div class="field">
                <?$propName = 'ASSIGNED_BY_ID';?>
                <label><small><?=$component->prefetchCorrectFieldName($propName, $arResult);?></small></label>
                    <?
                        $setup = [
                            "ID" => $propName,
                            "LAZYLOAD" => 'N',
                            "INPUT_NAME" => $propName,
                            "USE_SYMBOLIC_ID" => "Y",
                            "BUTTON_SELECT_CAPTION" => \Bitrix\Main\Localization\Loc::getMessage("ECLF_DESTINATION_ADD_USERS"),
                            "BUTTON_SELECT_CAPTION_MORE" => \Bitrix\Main\Localization\Loc::getMessage("ECLF_DESTINATION_ADD_MORE"),
                            "DUBLICATE" =>  "N",
                            "MULTIPLE"  =>  "N",
                            'API_VERSION' => 3,
                            'OPEN_DIALOG_WHEN_INIT' => true,
                            'ITEMS_SELECTED' => 'Y',
                            "SELECTOR_OPTIONS" => array(
                                'useNewCallback' => 'Y',
                                'lazyLoad' => 'N',
                                //'context' => $propName,
                                'contextCode' => 'U',
                                'enableUsers' => 'Y',
                                //'userSearchArea' => 'I', // E
                                'enableSonetgroups' => 'Y',
                                'enableDepartments' => 'Y',
                                'allowAddSocNetGroup' => 'N',
                                'departmentSelectDisable' => 'Y',
                                'showVacations' => 'Y',
                                'dublicate' => 'N',
                                'enableAll' => 'N',
                                'departmentFlatEnable' => 'Y',
                                'useSearch' => 'Y',
                                'multiple' => 'N',
                                'enableEmpty' => 'N'
                            )
                        ];
                        $setup['LIST'] = ['U'.$arResult['ENTITY_DATA']['ASSIGNED_BY_ID']];
                        $APPLICATION->IncludeComponent(
                            "bitrix:main.user.selector",
                            "",
                            $setup
                        );
                    ?>
            </div>
        </td>
        <td>
            <?$propName = 'SOURCE_ID';?>
            <div class="field">
                <label><small><?=$component->prefetchCorrectFieldName($propName, $arResult);?></small></label>
                <?=CrmCardShowExtComponent::createSelectUI($propName, $arResult, 5);?>
            </div>
        </td>
    </tr>

</table>