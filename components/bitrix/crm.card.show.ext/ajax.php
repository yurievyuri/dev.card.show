<?php
use Bitrix\Main\Config\Option;

define('NO_KEEP_STATISTIC', 'Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('STOP_STATISTICS', true);
define('PUBLIC_AJAX_MODE', true);
define('DisableEventsCheck', true);
require_once($_SERVER[ 'DOCUMENT_ROOT' ]. '/bitrix/modules/main/include/prolog_before.php' );

\Bitrix\Main\Loader::includeModule('main');
\Bitrix\Main\Loader::includeModule('crm');

$module_id = 'dev.card.show';
\Bitrix\Main\Loader::includeModule($module_id);
$prefix =  \Dev\Call\Card::prefix;

if ( $_REQUEST['BITRIX_SESSID'] != bitrix_sessid() ) die();

$return['results'] = false;
$return['error'] = true;

$groupsResponsible = explode(',', Option::get($module_id, $prefix . 'groups_resposible'));
$cache = \Bitrix\Main\Application::getInstance()->getManagedCache();
$cacheTtl = Option::get($module_id, $prefix . 'cache');
$debug = (Option::get($module_id, $prefix . 'debug') == 'Y') ? true : false;

$nameFormat = \CSite::getNameFormat(null, 'S1');

if ( isset($_REQUEST['saveLeadForm'] )) :

    $ID = isset($_REQUEST['ENTITY_ID']) ? intval($_REQUEST['ENTITY_ID']) : 0;

    if ( $_REQUEST['STATUS_ID']['JUNK'] )
        $_REQUEST['STATUS_ID'] = $_REQUEST['STATUS_ID']['JUNK'];

    $class = $_REQUEST['ENTITY_TYPE'] ? 'CCrm'.ucfirst(strtolower($_REQUEST['ENTITY_TYPE'])) : 'CCrmLead';
    if( $class && class_exists( $class) )
        $entity = new $class(false);

    //if(!$entity->CheckFields($fieldsForCheck, $ID))

    /*
        if ($_REQUEST['ENTITY_TYPE'] == 'LEAD')
            $entity = new CCrmLead(false);
        elseif ($_REQUEST['ENTITY_TYPE'] == 'CONTACT')
            $entity = new CCrmContact(false);
        elseif ($_REQUEST['ENTITY_TYPE'] == 'COMPANY')
            $entity = new CCrmCompany(false);
    */

    $new = new \Dev\Call\Functions($_REQUEST);

    $new->makeOutActions();

    $_REQUEST['STATUS_ID'] = $new->checkStatus();

    if ( !$new->getERROR() )
    {
        $return = $entity->Update(
            $ID,
            $_REQUEST,
            true,
            true,
            array(
                'DISABLE_USER_FIELD_CHECK' => true,
                'REGISTER_SONET_EVENT' => true,
                'ENABLE_SYSTEM_EVENTS' => true,
                'SYNCHRONIZE_STATUS_SEMANTICS' => true,
                'ENABLE_ACTIVITY_COMPLETION' => true
            )
        );

        if ( $entity->LAST_ERROR )
            $new->setERROR('entityUpdate', $entity->LAST_ERROR );
        else
        {
            try {

                $new->convertLeadToContact();

                if ( $new->getContactId() > 0 )
                {
                    // added update entity data
                    $new->update('CONTACT');

                    // added processing to bind other contacts
                    $lnk = new \Dev\Call\Link($_REQUEST);
                    $lnk->setParentId( $new->getContactId() );

                    $lnk->inboundProcessing();

                    if ( $lnk->getERROR() )
                        $new->setERROR( false, $lnk->getERROR() );
                }

            } catch (Exception $e) {

                $new->setERROR( false, $e );
            }
        }
    }

    if ( !$new->getERROR() )
        $new->deleteLead();

    if ( !$new->getRESULT() )
        $new->setRESULT( false, 'Data processed and successfully updated!' );

    $message = !$new->getERROR() ? $new->getRESULT() : $new->getERROR();
    if ( !is_string($message) )
        $message = implode('<br>', $message);

    $return = [
        'success'   => !$new->getERROR() ? true : false,
        'error'     => !$new->getERROR() ? false : true,
        'message'   =>  $message,
        //'post' => $entity->GetCheckExceptions()
    ];

elseif ( isset($_REQUEST['remoteuser']) ) :

    $cacheId = 'remoteUserList';
    if ($cache->read($cacheTtl, $cacheId) )
        $results = $cache->get($cacheId);
    else
    {
        $db = \Bitrix\Main\UserTable::getList([

            'order'     => ['NAME' => 'ASC'],
            'select'    => ['*'],
            'filter'    => [
                '!=UF_DEPARTMENT' => false,
                'ACTIVE'    => 'Y',

            ],
            'limit'     =>  1000,
            'cache' => [
                'ttl' => $cacheTtl,
                'cache_joins' => true
            ]
        ]);
        while($item = $db->Fetch())
        {
            $img = '';
            $name = \CUser::formatName($nameFormat, $item, true, false);

            /*if ( $item["PERSONAL_PHOTO"] )
            {
                $file = \CFile::GetFileArray($item["PERSONAL_PHOTO"]);
                if ($file !== false)
                {
                    $fileTmp = \CFile::ResizeImageGet(
                        $file,
                        array("width" => 50, "height" => 50),
                        BX_RESIZE_IMAGE_EXACT
                    )["src"];
                    $img = '<img class="ui avatar image" src="'.$fileTmp.'">';
                }
            }*/

            $results[] = [
                'name'  => $img . $name,
                'value' => $item['ID'],
                'text'  => $name,
                'disabled'  => false
            ];
        }

        if ( $results )
            $cache->set($cacheId, $results);
    }

    $return = [
        'success' => true,
        'results'   => $results,
        'error'     => $results ? false : true,
    ];

elseif (isset($_REQUEST['remoteusergroups'])) :

    $cacheId = 'remoteusergroups' . implode('-',$groupsResponsible);
    if ( $cache->read($cacheTtl, $cacheId) )
        $results = $cache->get($cacheId);
    else
    {
        $filter = [
            'ACTIVE'            => 'Y' ,
            'GROUP_ID'          => $groupsResponsible,
            '!UF_DEPARTMENT'    => false,
            '!NAME'             => false
        ];
        $db = CUser::GetList(($by="name"), ($order="asc"), $filter);

        while ($item = $db->Fetch())
        {
            $img = '';
            $name = \CUser::formatName($nameFormat, $item, true, false);
            if (!$name) continue;

            /*if ($item["PERSONAL_PHOTO"]) {
                $file = \CFile::GetFileArray($item["PERSONAL_PHOTO"]);
                if ($file !== false) {
                    $fileTmp = \CFile::ResizeImageGet(
                        $file,
                        array("width" => 50, "height" => 50),
                        BX_RESIZE_IMAGE_EXACT
                    )["src"];
                    $img = '<img class="ui avatar image" src="' . $fileTmp . '">';
                }
            }*/
            $results[] = [
                'name' => $img . $name,
                'value' => $item['ID'],
                'text' => $name,
                'disabled' => false
            ];
        }

        if ($results)
            $cache->set($cacheId, $results);
    }

    $return = [
        'success'   => true,
        'results'   => $results,
        'error'     => $results ? false : true,
    ];

elseif ( isset($_REQUEST['search']) ) :

    $return = $contactArray = \Dev\Call\Card::prepareArray( $_REQUEST['search'] );

    $return = [
        'success'   => $return ? true : false,
        'results'   => $return ? : false,
        'error'     => $return ? false : true,
    ];

endif;

$GLOBALS['APPLICATION']->RestartBuffer();
Header('Content-Type: application/json');
echo json_encode( $return );
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
die();